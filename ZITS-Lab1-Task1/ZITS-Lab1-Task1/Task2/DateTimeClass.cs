﻿using System;

namespace ZITS_Lab1_Task1.TaskSecond
{
    public class DateTimeClass
    {
        private DateTime _date;

        public bool IsLeapYear
        {
            get
            {
                return _date.Year % 4 == 0;
            }
        }

        public DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public DateTimeClass()
        {
            _date = new DateTime(2019, 01, 01);
        }

        public DateTimeClass(int year, int month, int day)
        {
            _date = new DateTime(year, month, day);
        }

        private DateTimeClass(DateTime dateTime)
        {
            _date = dateTime;
        }

        public DateTimeClass GetNextDay()
        {
            return new DateTimeClass(_date.AddDays(1));
        }

        public DateTimeClass GetPreviousDay()
        {
            return new DateTimeClass(_date.AddDays(-1));
        }

        public int GetDaysToMonthEnd()
        {
            var daysInMonth = DateTime.DaysInMonth(_date.Year, _date.Month);

            return daysInMonth - _date.Day;
        }
    }
}
