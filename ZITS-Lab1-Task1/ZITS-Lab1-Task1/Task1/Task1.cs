﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZITS_Lab1_Task1.TaskFirst
{
    public class Task1
    {
        private string _inputTimeFormat = "hh:mm:ss";
        private List<Train> _trains;
        private int _count = 2;

        public void StartTask()
        {
            _trains = new List<Train>();
            EnterData();
            _trains = _trains.OrderBy(x => x.Code).ToList();
            ShowAllTrains();
            ShowTrain();
        }

        private void EnterData()
        {
            for (var i = 0; i < _count; i++)
            {
                _trains.Add(EnterTrain());
            }
        }

        private Train EnterTrain()
        {
            Console.WriteLine("Enter train code: ");
            var code = Console.ReadLine();
            Console.WriteLine("\nEnter train destination: ");
            var destination = Console.ReadLine();
            Console.WriteLine($"\nEnter train Departure Time (format: {_inputTimeFormat}): ");
            var departureTime = Console.ReadLine();
            Console.Clear();

            var train = new Train();
            train.Code = long.Parse(code);
            train.DestinationName = destination;
            train.DepartureTime = TimeSpan.Parse(departureTime);

            return train;
        }

        private void ShowAllTrains()
        {
            foreach (var train in _trains)
            {
                Console.WriteLine($"{train.Code}\t {train.DepartureTime}\t {train.DestinationName}\n");
            }
        }

        private void ShowTrain()
        {
            Console.WriteLine("Enter train code: ");
            var code = long.Parse(Console.ReadLine());
            var train = _trains.FirstOrDefault(x => x.Code == code);
            if (train.Code != code)
            {
                Console.WriteLine("Train not found");
                Console.ReadLine();
                return;
            }
            Console.WriteLine($"{train.Code} {train.DestinationName} {train.DepartureTime}");
            Console.ReadLine();
        }
    }

}
