﻿using System;

namespace ZITS_Lab1_Task1.TaskFirst
{
    public struct Train
    {
        public string DestinationName { get; set; }
        public long Code { get; set; }
        public TimeSpan DepartureTime { get; set; }
    }
}
