﻿using ZITS_Lab1_Task1.Task3;
using ZITS_Lab1_Task1.TaskFirst;

namespace ZITS_Lab1_Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var task34 = new Task34();
            task34.StartTask();
            var task1 = new Task1();
            task1.StartTask();
        }
    }
}
