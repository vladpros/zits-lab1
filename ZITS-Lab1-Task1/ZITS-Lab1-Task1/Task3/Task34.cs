﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ZITS_Lab1_Task1.Task3
{
    public class Task34
    {
        public List<Client> Clients { get; set; } = new List<Client>();

        public void StartTask()
        {
            Clients = new List<Client> { new Kreditor { Name = "Client1", CreditDate = DateTime.UtcNow, }, new Org { Name = "Clinet2", OpenDate = DateTime.UtcNow.AddDays(-1) }, new Vkladchik { Name = "Client3", OpenDate = DateTime.UtcNow.AddDays(1) } };
            foreach(var client in Clients)
            {
                client.ShowClient();
                Console.WriteLine("\n");
            }

            Console.WriteLine("User with filter today:\n");

            foreach (var client in Clients.Where(x=> x.Search(DateTime.UtcNow)))
            {
                client.ShowClient();
                Console.WriteLine("\n");
            }
        }
    }

}
