﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZITS_Lab1_Task1.Task3
{
    public class Vkladchik : Client, IClient
    {
        public string Name { get; set; }
        public DateTime OpenDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? Rate { get; set; }

        public override bool Search(DateTime dateTime)
        {
            return dateTime.Date == OpenDate.Date;
        }

        public override void ShowClient()
        {
            Console.WriteLine($"Vkladcik - {Name}");
        }
    }
}
