﻿using System;

namespace ZITS_Lab1_Task1.Task3
{
    public abstract class Client
    {
        public abstract void ShowClient();
        public abstract bool Search(DateTime dateTime);
    }
}
