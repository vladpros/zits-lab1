﻿using System;

namespace ZITS_Lab1_Task1.Task3
{
    public class Org : Client, IClient
    {
        public string Name { get; set; }
        public DateTime OpenDate { get; set; }
        public string CardNumber { get; set; }
        public decimal? Balance { get; set; }

        public override bool Search(DateTime dateTime)
        {
            return dateTime.Date == OpenDate.Date;
        }

        public override void ShowClient()
        {
            Console.WriteLine($"Org - {Name}");
        }
    }
}
