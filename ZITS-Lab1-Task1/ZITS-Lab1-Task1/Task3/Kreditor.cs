﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZITS_Lab1_Task1.Task3
{
    public class Kreditor: Client, IClient
    {
        public string Name { get; set; }
        public DateTime CreditDate { get; set; }
        public string CardNumber { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Balance { get; set; }

        public override bool Search(DateTime dateTime)
        {
            return dateTime.Date == CreditDate.Date;
        }

        public override void ShowClient()
        {
            Console.WriteLine($"Kreditor - {Name}");
        }
    }
}
